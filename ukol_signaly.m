%% Na��t�n� a spojen� dataset� 
clear all;
clc;
eeg = importdata('eeg_05.mat');
hyp = importdata('hypnogram_05.mat');
nene={}
for j=1:length(eeg)/7500
    nene{j,1}=eeg(1:19,1+((j*7500)-7500):7500+((j*7500)-7500)); 
    nene{j,2}=hyp(j,3);
end
%% rozd�len� tr�novac� a testovac� 
clc
for i = 1:length(nene)
   for y = 1:2
       if(nene{i,2} > 0)
         hyp_test{i,y} = nene{i,y};
         Y{i,1} = nene{i,2};
       end
   end
end

a = 1;
for x = 1:length(nene)
   for z = 1:2
       if(nene{x,2} == 0)
         hyp_train{a,z} = nene{x,z};
         if(z == 2)
            a = a + 1;
         end
       end
   end
end
%% Vytvo�en� p��znak� pro celou mno�inu;
%clc
%
%for j = 19
%    for i=1:length(nene)
%        minimums(j,i)=min(nene{i}(j,:));
%        maximus(j,i)=max(nene{i}(j,:));
%        stds(j,i)=std(nene{i}(j,:));
%        sikmizmus(j,i)=skewness(nene{i}(j,:));
%        spicatizmus(j,i)=kurtosis(nene{i}(j,:));
%    end
%end
%% Vytvo�en� p��znak� train
clc

for j = 19
    for i=1:length(hyp_train)
        minimums_train(j,i)=min(hyp_train{i}(j,:));
        maximus_train(j,i)=max(hyp_train{i}(j,:));
        stds_train(j,i)=std(hyp_train{i}(j,:));
        prumers_train(j,i)=mean(hyp_train{i}(j,:));
        sikmizmus_train(j,i)=skewness(hyp_train{i}(j,:));
        spicatizmus_train(j,i)=kurtosis(hyp_train{i}(j,:));
    end
end
%% Vytvo�en� p��znak� test
clc

for j = 19
    for i=1:length(hyp_test)
        minimums_test(j,i)=min(hyp_test{i}(j,:));
        maximus_test(j,i)=max(hyp_test{i}(j,:));
        stds_test(j,i)=std(hyp_test{i}(j,:));
        prumers_test(j,i)=mean(hyp_test{i}(j,:));
        sikmizmus_test(j,i)=skewness(hyp_test{i}(j,:));
        spicatizmus_test(j,i)=kurtosis(hyp_test{i}(j,:));
    end
end
%% tady dotv���m p��znaky pro test
for i=1:19
    if i ==1
        min=minimums_test(i,:);
        max=maximus_test(i,:);
        std=stds_test(i,:);
        prm=prumers_test(i,:);
        sikm=sikmizmus_test(i,:);
        spic=spicatizmus_test(i,:);
    elseif i<19
        min=[min, minimums_test(i+1,:)];
        max=[max, maximus_test(i+1,:)];
        std=[std, stds_test(i+1,:)];
        prm=[prm, prumers_test(i+1,:)];
        sikm=[sikm, sikmizmus_test(i+1,:)];
        spic=[spic, spicatizmus_test(i+1,:)];
    else
        break
    end
end
%% tady dotv���m p��znaky pro train
for i=1:19
    if i ==1
        min_train=minimums_train(i,:);
        max_train=maximus_train(i,:);
        std_train=stds_train(i,:);
        prm_train=prumers_train(i,:);
        sikm_train=sikmizmus_train(i,:);
        spic_train=spicatizmus_train(i,:);
    elseif i<19
        min_train=[min_train, minimums_train(i+1,:)];
        max_train=[max_train, maximus_train(i+1,:)];
        std_train=[std_train, stds_train(i+1,:)];
        prm_train=[prm_train, prumers_train(i+1,:)];
        sikm_train=[sikm_train, sikmizmus_train(i+1,:)];
        spic_train=[spic_train, spicatizmus_train(i+1,:)];
    else
        break
    end
end
%% Vytvo�en� datasetu pro testov�n�
clc
test_data=struct('Min',min,'Max',max,'Std',std,'Prumer',prm,'Sikmost',sikm,'Spicatost',spic);
test_train=struct('Min',min_train,'Max',max_train,'Std',std_train,'Prumer',prm_train,'Sikmost',sikm_train,'Spicatost',spic_train);
%% Normalizace
clc
pomocnikc(1,:) = prumers_test(19,:);
pomocnikc(2,:) = minimums_test(19,:);
pomocnikc(3,:) = maximus_test(19,:);
pomocnikc(4,:) = sikmizmus_test(19,:);
pomocnikc(5,:) = spicatizmus_test(19,:);
pomocnikc(6,:) = stds_test(19,:);
X = pomocnikc().';
pomocnik(1,:) = prumers_train(19,:);
pomocnik(2,:) = minimums_train(19,:);
pomocnik(3,:) = maximus_train(19,:);
pomocnik(4,:) = sikmizmus_train(19,:);
pomocnik(5,:) = spicatizmus_train(19,:);
pomocnik(6,:) = stds_train(19,:);
trenacek = pomocnik().';

YS=cell2mat(Y);
%% test
clc
Mdl = fitcknn(X,YS,'NumNeighbors',4)
pred=predict(Mdl,trenacek);
CVMdl = crossval(Mdl,'KFold',5);
kloss = kfoldLoss(CVMdl)


%%
clc
z = 1;
for i = 1:length(nene)
   A = pred(z);
   for y = 1:2
       if(nene{i,2} > 0)
         konec{i,y} = nene{i,y};
       elseif(nene{i,2}==0)  
         konec{i,1} = nene{i,1};
         konec{i,2} = A;
         if(z<length(trenacek))
         z=z+1;
         end
       end
   end
end

save('hypnogram_5.mat','konec')  
%%
hypno = importdata('hypnogram_5.mat');